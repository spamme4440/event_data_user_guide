# Faculty Opinions

| | |
|---------------------------|-|
| Agent Source token        | `f66ad8fc-4def-4f99-9d59-62246e604534` |
| Consumes Artifacts        |  |
| Subject coverage          | Faculty Opinions Reviews that relate to articles with DOIs. |
| Object coverage           | All DOIs. |
| Data contributor          | Faculty Opinions  |
| Data origin               | Faculty Opinions |
| Freshness                 | Every 10 days |
| Identifies                | Linked DOIs |
| License                   | Creative commons [CC0 1.0 Universal (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/) |
| Looks in                  | Faculty Opinions's links |
| Name                      | Faculty Opinions |
| Operated by               | Crossref |
| Produces Evidence Records | Yes |
| Produces relation types   | `reviews` |
| Source ID                 | `facultyopinions` |
| Updates or deletions      | None expected |

## What it is

Faculty Opinions is an online review publisher. Each Faculty Opinions Review can be related to a DOI. The Faculty Opinions source records the links between Faculty Opinions reviews and the articles that they review.

## What it does

The Agent extracts links from the Faculty Opinions database. Faculty Opinions assign DOIs to their reviews and also use DOIs to refer to articles. 

## Where data comes from

Faculty Opinions provide Crossref with a periodic data dump. 

## Example Event

    {
      "license": "https://creativecommons.org/publicdomain/zero/1.0/",
      "obj_id": "https://doi.org/10.1073/pnas.222371199",
      "source_token": "85dce7cf-7bbf-40c0-a983-739a784db09a",
      "occurred_at": "2002-11-07T00:00:00Z",
      "subj_id": "https://doi.org/10.3410/f.1010337.150911",
      "id": "fe64b40e-a69d-4f67-8918-f148bd4d9e19",
      "evidence_record": "https://evidence.eventdata.crossref.org/evidence/20180207-facultyopinions-6731c645-53f6-47b9-b4ea-8cd8984a9826",
      "terms": "https://doi.org/10.13003/CED-terms-of-use",
      "action": "add",
      "subj": {
        "pid": "https://doi.org/10.3410/f.1010337.150911",
        "url": "https://doi.org/10.3410/f.1010337.150911",
        "alternative_id": "150911",
        "work_type_id": "review"
      },
      "source_id": "facultyopinions",
      "obj": {
      "pid": "https://doi.org/10.1073/pnas.222371199",
        "url": "https://doi.org/10.1073/pnas.222371199"
      },
      "timestamp": "2018-02-07T18:00:37Z",
      "relation_type_id": "reviews"
    }


## Methodology

Every 10 days the Agent retrieves the Faculty Opinions data dump and extracts all new links.

## Evidence Record

 - Includes batches of `url` type observations that correspond to the Faculty Opinions dump.

## Edits / deletion

 - Events may be edited if they are found to be faulty, e.g. non-existent DOIs

## Quirks

Faculty Opinions was previously named F1000 and the agent name was updated in June 2021.

## Failure modes

 - Faculty Opinions data dump might not be up to date.

## Further information

 - [Faculty Opinions](https://facultyopinions.com/)
