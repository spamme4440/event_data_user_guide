# Crossref metadata

## Datacite

| | |
|---------------------------|-|
| Agent Source token        | `8676e950-8ac5-4074-8ac3-c0a18ada7e99` |
| Consumes Artifacts        | none |
| Subject coverage          | All Crossref DOIs |
| Object coverage           | All DataCite DOIs |
| Data contributor          | Crossref |
| Data origin               | Crossref metadata deposited by Crossref members |
| Freshness                 | Every few hours |
| Identifies                | DOIs |
| License                   | Made available without restriction |
| Looks in                  | References in Crossref schema |
| Name                      | Crossref metadata |
| Operated by               | Crossref |
| Produces Evidence Records | No |
| Produces relation types   | `references` and other types found in the Crossref schema |
| Source ID                 | `crossref` |
| Updates or deletions      | None expected |

### What it is

Relationships from Crossref DOIs to DataCite DOIs. The data is supplied in the metadata of Crossref's registered content by Crossref members who are the 'owners' of the registered content.

### What it does

Where members include DataCite DOIs in references the Crossref Event Data agent establishes the relationship between the DataCite DOI and the Crossref DOI as an event.

### Where data comes from

Metadata registered with Crossref that references a DataCite DOI. For other Crossref member metadata, you can find it in the [Crossref Metadata REST API](https://www.crossref.org/education/retrieve-metadata/rest-api/).


### Example Event

    {
      "obj_id":"https://doi.org/10.13127/ITACA/2.1",
      "occurred_at":"2016-08-19T20:30:00Z",
      "subj_id":"https://doi.org/10.1007/S10518-016-9982-8",
      "total":1,
      "id":"8676e950-8ac5-4074-8ac3-c0a18ada7e99",
      "message_action":"create",
      "source_id":"crossref",
      "timestamp":"2016-08-19T22:14:33Z",
      "terms": "https://doi.org/10.13003/CED-terms-of-use",
      "relation_type_id":"cites"
    }

### Methodology

1. A member makes an XML deposit with Crossref.
2. The Agent monitors new deposits (or back-fills).
3. When a link is found an Event is created if:
    - it is to a non-Crossref DOI
    - it has not already been reported to Event Data

### Evidence Record

No evidence record is created because the events come straight from the data source into Event Data.

### Edits / deletion

We do not anticipate that Events are ever deleted or edited.

### Quirks

The agent doesn't monitor unstructured references, they must be well-formatted with a specified type to be picked up by the agent.

### Failure modes

 - Member may remove references before the Agent first scans the data, in which case we will not create a new Event
 - Member may deposit incorrect metadata.

### Further information

 - [Crossref Schema documentation](https://www.crossref.org/education/content-registration/crossrefs-metadata-deposit-schema/)

## Crossref to Crossref relationships and references

| | |
|---------------------------|-|
| Agent Source token        | `36c35e23-8757-4a9d-aacf-345e9b7eb50d` |
| Consumes Artifacts        | none |
| Subject coverage          | All Crossref DOIs |
| Object coverage           | All Crossref DOIs |
| Data contributor          | Crossref |
| Data origin               | Crossref metadata deposited by Crossref members |
| Freshness                 | Daily |
| Identifies                | DOIs |
| License                   | Made available without restriction |
| Looks in                  | Relationships in Crossref schema |
| Name                      | Crossref metadata |
| Operated by               | Crossref |
| Produces Evidence Records | No |
| Produces relation types   | `references` and other types found in the Crossref schema |
| Source ID                 | `crossref` |
| Updates or deletions      | None expected |

### What it is

Relationships and references from Crossref DOIs to Crossref DOIs. The data is supplied in the metadata of Crossref's registered content by Crossref members who are the 'owners' of the registered content.

### What it does

#### Relationships
Where members include relationships (where the type is `doi`) the Crossref Event Data agent establishes the relationship between the parent DOI and the related DOI as an event.

#### References
The agent creates events for all references between a parent DOI and any reference that has a DOI or unstructured text.

### Where data comes from

Metadata registered with Crossref that have a relationship to an object with a type of `doi` or have references or both. For other Crossref member metadata, you can find it in the [Crossref Metadata REST API](https://www.crossref.org/education/retrieve-metadata/rest-api/).


### Example Event

    {
    "license": "https://creativecommons.org/publicdomain/zero/1.0/",
    "obj_id": "https://doi.org/10.5194/bgd-9-13537-2012",
    "source_token": "36c35e23-8757-4a9d-aacf-345e9b7eb50d",
    "occurred_at": "2021-04-30T23:08:31.000Z",
    "subj_id": "https://doi.org/10.5194/bg-10-513-2013",
    "id": "8956f975-ea29-49d3-bae4-35b457e7b6a3",
    "evidence_record": "https://evidence.eventdata.crossref.org/evidence/20210430-metadata-c2cc8585-5ca3-4239-b8e0-640b4b59757e",
    "action": "add",
    "subj": {
        "pid": "https://doi.org/10.5194/bg-10-513-2013",
        "url": "https://doi.org/10.5194/bg-10-513-2013",
        "work_type_id": "journal-article"
    },
    "source_id": "crossref",
    "obj": {
        "pid": "https://doi.org/10.5194/bgd-9-13537-2012",
        "url": "https://doi.org/10.5194/bgd-9-13537-2012",
        "method": "doi-literal",
        "verification": "literal",
        "work_type_id": "posted-content"
    },
    "timestamp": "2021-05-06T07:33:56Z",
    "relation_type_id": "has-preprint"
}

### Methodology

1. A member makes an XML deposit with Crossref.
2. The Agent monitors new deposits (or back-fills).
3. When a DOI with one or more relationships or references is found an Event is created if:
    - it is a relationship to an object with a type of doi
    - It is a reference with a doi or unstructured text
    - it has not already been reported to Event Data

### Evidence Record

No evidence record is created because the events come straight from the data source into Event Data.

### Edits / deletion

We do not anticipate that Events are ever deleted or edited.
