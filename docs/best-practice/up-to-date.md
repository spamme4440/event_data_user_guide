# Keep up-to-date with Event Data

Once an Event has been created, we won't remove it from our system unless absolutely necessary. However, there are some circumstances where we may edit it or mark it as deleted. We try to avoid editing Events once they are created but in some cases, notably Twitter, where we were obliged to remove all data when our contract ended.

If you retrieve Events from Event Data, or if you perform calculations based on data from Event Data, you should check back to make sure the original data hasn't changed. Examples of when you might like to check:

 - If you are publishing a research paper, make a final check before publication.
 - If you producing on-going data derived from Event Data, you may want to re-check every month or so.

Data can change for a number of reasons, such as: if we ran a report that indicated we produced obviously incorrect data as a result of a software bug, we may mark Events as having been deleted or edited.

When an Event is updated, we will add the an `updated` field, an `updated-date` field and an `updated-reason`. The `updated` field will have a value of:

 - `deleted` when we identified obviously faulty data.
 - `edited` when we identified obviously faulty data but it can be corrected.

For further discussion see [updates](/guide/data/updates). For more details of how to do this check the [Query API](/guide/service/query-api/) documentation.
